//  Dividi - an automation tool and media library management.
// Copyright (C) 2023 Camilo Q.S. (Distopico) <distopico@riseup.net>
//
// This file is part of Dividi.
//
// Dividi is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

mod cli;

fn main() {
    let _cli_options = cli::DividiCli::new();
}
